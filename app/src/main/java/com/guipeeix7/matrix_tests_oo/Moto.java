package com.guipeeix7.matrix_tests_oo;

public class Moto extends Veiculos{
    public Moto() {
        setTipo_de_veiculo("Moto");
        setRendimento(50);
        setCargaMaxima(50);
        setVelocidadeMedia(110);
        setPerda_de_rendimento(0.3);
        Define_combustivel("Gasolina","Alcool");
        setDisponibilidade(1);
    }
}
