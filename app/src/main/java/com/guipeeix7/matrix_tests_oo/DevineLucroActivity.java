package com.guipeeix7.matrix_tests_oo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class DevineLucroActivity extends AppCompatActivity {

    private EditText edittextlucro;

    public void On_click_set_Lucro(View view){
        if(edittextlucro.getText().length() == 0)
            Toast.makeText(this, "Por favor defina uma margem de lucro...", Toast.LENGTH_SHORT).show();
        else{
            double Lucro = Double.parseDouble(edittextlucro.getText().toString());
            Intent backMain = new Intent(DevineLucroActivity.this,MainActivity.class);
            backMain.putExtra("LUCRO" , Lucro);
            setResult(RESULT_OK,backMain);
            finish();
        }

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_devine_lucro);
        edittextlucro = findViewById(R.id.editTextLucro);

    }
}
