package com.guipeeix7.matrix_tests_oo;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


///LEMBRAR DE ALTERAR O MEU CONTADOR DE CARROS
//FAZER OS CALCULOS ....

public class MainActivity extends AppCompatActivity {
///////////////////////////////////////////////VARIAVEIS DA ATIVIDADE PRINCIPAL/////////////////////////////////////////////////////////////////////////////
    private ImageButton add_vehicles;
    private TextView text_cars;
    private TextView textViewCarretaDisponiveis,textViewCarretaOcupadas,textViewCarrosDisponiveis,textViewCarrosOcupados,textViewVansDisponiveis,textViewVansOcupadas,textViewMotosDisponiveis,textViewMotosOcupadas;

    private GridLayout gridcars;


    private TextView textviewCustodeOperacao, textviewCustoeLucro, textviewTempoDeOpecacao, texviewVeiculoOperacao,
                     textviewMaisRapido,  textviewVeiculoMaisRapido, textviewTempoMaisRapido, texviewLucroMaisRapido,
                     textviewmelhorCB, textviewCB, textviewTempoCB, texviewVeiculoLucroCB;
    private TextView textviewMoney;
   private ImageView imageviewCustoDeOperacao,
                      imageviewMaisRapido,
                      imageviewCB;
    private Button buttonSelectMenorCusto,
                   bUttonTempo,
                   buttonSelectCB;
    private TextView logCars;
    private TextView textviewPeso, textviewDistancia, textviewTempoMax;
    Seu_ze primeiro_zezinho;Carreta my_Carretas[];Van my_Vans[];Carro my_Carros[];Moto my_Motos[];
    public Carreta carreta_teste = new Carreta();
    public Carro carro_teste = new Carro();
    public Van van_teste = new Van();
    public Moto moto_teste = new Moto();
    public Veiculos VeiculosTeste[] = new Veiculos[4];
    boolean VeiculoCapaz[] = new boolean[4];
    public int[] menor_index= new int[4]; //Recebe a variavel da funcao
    double TaxadeLucroDoZe;
    int Comeca_instanciacao = 0;
    int []Incrementa = new int[4];
    int numero_de_carretas ,numero_de_carros , numero_de_vans, numero_de_motos;
    private double Peso,Distancia,Tempo_maximo;
    double Custos[] = new double[4];
    public String StringLogCars = " ";
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////?SALVANDO DADOS APOS O FECHAR DO APP///////////////////////////////////////////
    public static final String SHARED_PREFS = "sharedPrefs";
    public static final String PESO = "peso";
    public static final String DISTANCIA = "distancia";
    public static final String TEMPO_MAXIMO = "tempo_maximo";

    public static final String CARRETAS = "carretas";
    public static final String CARROS = "carros";
    public static final String VANS = "vans";
    public static final String MOTOS = "motos";

    public static final String LUCRO = "lucro";
    public static final String LOGVEICULOS = "logveiculos";
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////FUNCOES DA ATIVIDADE PRINCIPAL/////////////////////////////////////////////////////////////////////////////
    public void DadosDoUsuario(View view){
        Intent dadosIntent = new Intent(MainActivity.this, DadosDoCLiente.class);
        startActivityForResult(dadosIntent, 2); //Because we want thigs back from it.
    }
    public void Definelucroactivity(View view){
        Intent lucroIntent = new Intent(MainActivity.this, DevineLucroActivity.class);
        startActivityForResult(lucroIntent, 3); //Because we want thigs back from it.
    }
    public void openAddvehiclesActivity(View view) {
        Intent vehiclesIntent = new Intent(MainActivity.this, Add_vehicles.class);
        startActivityForResult(vehiclesIntent, 1); //Because we want thigs back from it.
        Comeca_instanciacao = 1;
    }
    public void funcoes_teste(int numero_de_carretas[]){
        Log.i("TESTE4", String.valueOf(numero_de_carretas[0]));
        int i;
        for(i = 0 ; i <  numero_de_carretas[0] ; i++){
            Log.i("TESTE4", String.valueOf(my_Carretas[i].getDisponibilidade()));
        }
        Toast.makeText(this, String.valueOf(i), Toast.LENGTH_SHORT).show();
    }
    public void setEncomendaTexts(double peso, double distancia,double tempo) {
        textviewPeso.setText(String.valueOf(peso));
        textviewDistancia.setText(String.valueOf(distancia));
        textviewTempoMax.setText(String.valueOf(tempo));
    }
    public void InstanciaOsBagulho(){
        my_Carretas = new Carreta[primeiro_zezinho.getN_cars()[0]];
        my_Carros = new Carro[primeiro_zezinho.getN_cars()[1]];
        my_Vans = new Van[primeiro_zezinho.getN_cars()[2]];
        my_Motos = new Moto[primeiro_zezinho.getN_cars()[3]];

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < primeiro_zezinho.getN_cars()[i]; j++) {
                if (i == 0) {my_Carretas[j] = new Carreta();}
                if (i == 1) {my_Carros[j] = new Carro();}
                if (i == 2) {my_Vans[j] = new Van();}
                if (i == 3) {my_Motos[j] = new Moto();}
            }
            Comeca_instanciacao = 1;
            //Toast.makeText(this, "Objetos instanciados", Toast.LENGTH_SHORT).show();
        }
        updateViewsCars();

    }
    public double menorCustoDeVIajem (){
        menor_index[0] = 5;
        double menor_preco = Custos[0];
        for(int i = 0 ; i < 4 ; i++){
            if(menor_preco >= Custos[i] && VeiculoCapaz[i]){
                menor_preco = Custos[i];
                menor_index[0] = i;
            }
        }

      Log.i("PRECOS", "Este e o preco "+ menor_preco +"index = "+ menor_index);

        changeimagesandtexts(menor_index[0],0);
        if(menor_index[0] == 5) {
            return -1;
        }
        return 0;
    }
    public double menorTempoDeViajem(){
        menor_index[1] = 5;

        if(VeiculoCapaz[3] == true){
            menor_index[1] = 3;
            changeimagesandtexts(menor_index[1],1);
            return 0;
        }
        if(VeiculoCapaz[1] == true){
            menor_index[1] = 1;
            changeimagesandtexts(menor_index[1],1);
            return 0;
        }
        if(VeiculoCapaz[2] == true){
            menor_index[1] = 2;
            changeimagesandtexts(menor_index[1],1);
            return 0;
        }
        if(VeiculoCapaz[0] == true){
            menor_index[1] = 0;
            changeimagesandtexts(menor_index[1],1);
            return 0;
        }

        return 5;
    }
    public int MelhorCustoBeneficio(){
        double []CB = new double[4];
        for(int i =0 ; i < 4 ; i++){
            CB[i] = Custos[i]*(1+(TaxadeLucroDoZe/100));
            CB[i] = CB[i] - Custos[i];
        }

        menor_index[2] = 5;
        double melhorCB = CB[3];

        for(int i = 0 ; i < 4 ; i++){
            if(melhorCB <= CB[i] && VeiculoCapaz[i]){
                menor_index[2] = i;
                melhorCB = CB[i];
            }
        }
        changeimagesandtexts(menor_index[2],2);

        if(menor_index[2] == 5) {
            return -1;
        }

        return 0;
    }
    public void errorLog(){
        Toast.makeText(this, "Nao existem veiculos que atendam os requisitos!", Toast.LENGTH_SHORT).show();
    }
    public boolean[] checa_condicoes() {

        Log.i("CHAMADO", "Chamada de funcoes");
        boolean checa_disponibilidades[] = new boolean[4];
        for (int i = 0; i < 4; i++){ checa_disponibilidades[i] = false; VeiculoCapaz[i] = false;}


        for (int i = 0; i < 4; i++) {

            Log.i("CHEKAGEM", "\n\n");
            for (int j = 0; j < primeiro_zezinho.getN_cars()[i]; j++) {
                if (i == 0) {
                    if(my_Carretas[j].getDisponibilidade() == 1);
                        checa_disponibilidades[0] = true; break;
                }
                if (i == 1) {
                    if(my_Carros[j].getDisponibilidade() == 1);
                        checa_disponibilidades[1] = true; break;
                }
                if (i == 2 ) {
                    if(my_Vans[j].getDisponibilidade() == 1);
                       checa_disponibilidades[2] = true; break;
                }
                if (i == 3 ) {
                    if(my_Motos[j].getDisponibilidade() == 1);
                        checa_disponibilidades[3] = true; break;
                }
            }

        }


        if (carreta_teste.getCargaMaxima() >= Peso && carreta_teste.getVelocidadeMedia() > (Distancia / Tempo_maximo) && checa_disponibilidades[0] == true) {
            VeiculoCapaz[0] = true;
        }
        Log.i("CHEK_COND", "Checa Carreta:"+VeiculoCapaz[0]+ "Peso = " + Peso);
        if (carro_teste.getCargaMaxima() >= Peso && carro_teste.getVelocidadeMedia() >= (Distancia / Tempo_maximo) && checa_disponibilidades[1] == true) {
            VeiculoCapaz[1] = true;
        }

        Log.i("CHEK_COND", "Checa carro:"+VeiculoCapaz[1]);
        if (van_teste.getCargaMaxima() >= Peso && van_teste.getVelocidadeMedia() >= (Distancia / Tempo_maximo) && checa_disponibilidades[2] == true) {
            VeiculoCapaz[2] = true;
        }

        Log.i("CHEK_COND", "Checa Van:"+VeiculoCapaz[2]);
        if (moto_teste.getCargaMaxima() >= Peso && moto_teste.getVelocidadeMedia() >= (Distancia / Tempo_maximo) && checa_disponibilidades[3] == true) {
            VeiculoCapaz[3] = true;
        }

        Log.i("CHEK_COND", "Checa Moto:"+VeiculoCapaz[3]);
        return VeiculoCapaz;
    }
    public int changeimagesandtexts(int index , int cod){

        changeVehicleImage(index,cod);
        changeVehicleText(index,cod);
        changeTImeText(index,cod);
        changeMoneyText(index, cod);
        return 0;
    }
    public int checaNumeroDeVeiculosOcupados(int index){
        int ndisponiveis_in_index = 0;
        for (int j = 0; j < primeiro_zezinho.getN_cars()[index]; j++) {
            if(index == 0 &&  my_Carretas[j].getDisponibilidade()== 0){
                ndisponiveis_in_index++;
            }
            if(index == 1 &&  my_Carros[j].getDisponibilidade()== 0){
                ndisponiveis_in_index++;
            }
            if(index == 2 &&  my_Vans[j].getDisponibilidade()== 0){
                ndisponiveis_in_index++;
            }
            if(index == 3 &&  my_Motos[j].getDisponibilidade()== 0){
                ndisponiveis_in_index++;
            }
        }
        return ndisponiveis_in_index;
    }
    public void OnclicSelect(View view) {
        Button mySO = (Button) view;

        ////////////////////////////TAKING A TAG///////////////////////
        String tag = String.valueOf(mySO.getTag());
        int tagIndex = Integer.parseInt(tag);
        System.out.println(tagIndex);
        //////////////////////////////////////////////////////////////

        for(int i = 0 ; i <4 ; i++) {
            if (tagIndex == i) {
                if (menor_index[0] == 0) {

                    for (int k = 0; k < primeiro_zezinho.getN_cars()[0]; k++) {

                        if (my_Carretas[k].getDisponibilidade() == 1) {

                            my_Carretas[k].setDisponibilidade(0);
                            textViewCarretaOcupadas.setText(String.valueOf(checaNumeroDeVeiculosOcupados(0)));
                            gridcars.animate().translationY(-1000).setDuration(4000);
                            StringLogCars+="O veiculo escolhido para tranporte foi a: " +"Carreta. "+ "O tempo total de entrega e de: " + Distancia/carreta_teste.getVelocidadeMedia()+"h " + "O custo total e de:" + Custos[0]*(1+(TaxadeLucroDoZe/100)) +"\n";
                            logCars.setText(StringLogCars);

                            return;
                        }
                    }
                }
                if (menor_index[0] == 1) {
                    for (int k = 0; k < primeiro_zezinho.getN_cars()[1]; k++) {
                        if (my_Carros[k].getDisponibilidade() == 1) {
                            my_Carros[k].setDisponibilidade(0);
                            textViewCarrosOcupados.setText(String.valueOf(checaNumeroDeVeiculosOcupados(1)));
                            gridcars.animate().translationY(-1000).setDuration(4000);
                            StringLogCars+="O veiculo escolhido para tranporte foi o:" +"Carro. "+ "O tempo total de entrega e de: " + Distancia/carro_teste.getVelocidadeMedia()+"h " + "O custo total e de:" + Custos[1]*(1+(TaxadeLucroDoZe/100)) +"\n";
                            logCars.setText(StringLogCars);

                            return;
                        }
                    }
                }
                if (menor_index[0] == 2) {
                    for (int k = 0; k < primeiro_zezinho.getN_cars()[2]; k++) {
                        if (my_Vans[k].getDisponibilidade() == 1) {
                            my_Vans[k].setDisponibilidade(0);
                            gridcars.animate().translationY(-1000).setDuration(4000);
                            textViewVansOcupadas.setText(String.valueOf(checaNumeroDeVeiculosOcupados(2)));
                            StringLogCars+="O veiculo escolhido para tranporte foi a: " +"Van. "+ "O tempo total de entrega e de: " + Distancia/carro_teste.getVelocidadeMedia()+"h " + "O custo total e de:" + Custos[0]*(1+(TaxadeLucroDoZe/100)) +"\n";
                            logCars.setText(StringLogCars);
                            return;
                        }
                    }
                }
                if (menor_index[0] == 3) {
                    for (int k = 0; k < primeiro_zezinho.getN_cars()[3]; k++) {
                        if (my_Motos[k].getDisponibilidade() == 1) {
                            my_Motos[k].setDisponibilidade(0);
                            gridcars.animate().translationY(-1000).setDuration(4000);
                            textViewMotosOcupadas.setText(String.valueOf(checaNumeroDeVeiculosOcupados(3)));
                            StringLogCars+="O veiculo escolhido para tranporte foi o(a): " +"Moto. "+ "O tempo total de entrega e de: " + Distancia/carreta_teste.getVelocidadeMedia() +"h " + "O custo total e de:" + Custos[0]*(1+(TaxadeLucroDoZe/100)) +"\n";
                            logCars.setText(StringLogCars);
                            return;
                        }
                    }
                }
            }
        }
        errorLog();
    }
    public int changeVehicleImage(int index, int cod){
        if(index == 5){
            errorLog();

            return 1;
        }

        if(cod == 0) {

            if (index == 0) imageviewCustoDeOperacao.setImageResource(R.drawable.truck_24dp);

            if (index == 1) imageviewCustoDeOperacao.setImageResource(R.drawable.car_24dp);

            if (index == 2) imageviewCustoDeOperacao.setImageResource(R.drawable.van_24dp);

            if (index == 3) imageviewCustoDeOperacao.setImageResource(R.drawable.moto_24dp);
        }
        if(cod == 1) {
            if (index == 0) imageviewMaisRapido.setImageResource(R.drawable.truck_24dp);

            if (index == 1) imageviewMaisRapido.setImageResource(R.drawable.car_24dp);

            if (index == 2) imageviewMaisRapido.setImageResource(R.drawable.van_24dp);

            if (index == 3) imageviewMaisRapido.setImageResource(R.drawable.moto_24dp);
        }
        if(cod == 2) {
            if (index == 0) imageviewCB.setImageResource(R.drawable.truck_24dp);

            if (index == 1) imageviewCB.setImageResource(R.drawable.car_24dp);

            if (index == 2) imageviewCB.setImageResource(R.drawable.van_24dp);

            if (index == 3) imageviewCB.setImageResource(R.drawable.moto_24dp);
        }
        return 0;
    }
    public int changeVehicleText(int index, int cod){
        if(index == 5){
            errorLog();
            return 0;
        }
        String stringN = "Veiculo:";
         String VheiclesString[] = {"Carreta","Carro","Van","Moto"};
        if(cod == 0){
            if(index == 0 || index == 1 || index == 2 || index == 3) {
                textviewCustoeLucro.setText(stringN+" " +VheiclesString[index]);

            }
        }
        if(cod == 1){
            if(index == 0 || index == 1 || index == 2 || index == 3) {
                textviewVeiculoMaisRapido.setText(stringN + " " + VheiclesString[index]);
            }
        }
        if(cod == 2){
            if(index == 0 || index == 1 || index == 2 || index == 3) {
                textviewCB.setText(stringN + " " + VheiclesString[index]);
            }
        }



        return 0;
    }
    public int changeTImeText(int index, int cod){
        double[] Tempo = new double[4];
        Tempo[0] = Distancia/carreta_teste.getVelocidadeMedia();
        Tempo[1] = Distancia/carro_teste.getVelocidadeMedia();
        Tempo[2] = Distancia/van_teste.getVelocidadeMedia();
        Tempo[3] = Distancia/moto_teste.getVelocidadeMedia();
        String stringP = "Duracao:";
        if(cod == 0){
            if(index == 0 || index == 1 || index == 2 || index == 3)
                textviewTempoDeOpecacao.setText(stringP+" "+String.format("%.2f",Tempo[index])+"h");
        }
        if(cod == 1){
            if(index == 0 || index == 1 || index == 2 || index == 3)
                textviewTempoMaisRapido.setText(stringP+" " +String.format("%.2f",Tempo[index])+ "h");
        }
        if(cod == 2){
            if(index == 0 || index == 1 || index == 2 || index == 3)
                textviewTempoCB.setText(stringP+" " +String.format("%.2f",Tempo[index])+ "h");
        }

        return 0;
    }
    public int changeMoneyText(int index, int cod){
        String stringM = "$+Lucro = ";

        if(cod == 0){
            texviewVeiculoOperacao.setText(stringM + String.format("%.2f",Custos[index]*(1+(TaxadeLucroDoZe/100))));
        }
        if(cod == 1){
            texviewLucroMaisRapido.setText(stringM + String.format("%.2f",Custos[index]*(1+(TaxadeLucroDoZe/100))));
        }
        if(cod == 2){
            texviewVeiculoLucroCB.setText(stringM + String.format("%.2f",Custos[index]*(1+(TaxadeLucroDoZe/100))));
        }
        return 0;
    }
////////////////////////////////////////////////SAVING DATA///////////////////////////////////////////////////////////
    public void saveData(View view){
          SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
          SharedPreferences.Editor editor = sharedPreferences.edit();
          editor.putFloat(PESO,(float)Peso);
          editor.putFloat(DISTANCIA,(float)Distancia);
          editor.putFloat(TEMPO_MAXIMO,(float)Tempo_maximo);
          editor.putFloat(LUCRO,(float)TaxadeLucroDoZe);

          editor.putInt(CARRETAS, numero_de_carretas);
          editor.putInt(CARROS, numero_de_carros);
          editor.putInt(VANS, numero_de_vans);
          editor.putInt(MOTOS, numero_de_motos);

          editor.putString(LOGVEICULOS,StringLogCars);

          editor.apply();
    }
    public void loadData(){
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        Peso = sharedPreferences.getFloat(PESO, 0);
        Distancia = sharedPreferences.getFloat(DISTANCIA,0);
        Tempo_maximo = sharedPreferences.getFloat(TEMPO_MAXIMO,0);
        TaxadeLucroDoZe = sharedPreferences.getFloat(LUCRO,0);

        numero_de_carretas = sharedPreferences.getInt(CARRETAS, 0);
        numero_de_carros = sharedPreferences.getInt(CARROS, 0);
        numero_de_vans = sharedPreferences.getInt(VANS, 0);
        numero_de_motos = sharedPreferences.getInt(MOTOS, 0);
        primeiro_zezinho.setN_cars(numero_de_carretas,numero_de_carros,numero_de_vans,numero_de_motos);

        StringLogCars = sharedPreferences.getString(LOGVEICULOS," ");

    }
    public void updateViewsEncomendas() {
        textviewPeso.setText(String.valueOf(String.format("%.2f",Peso)));
        textviewDistancia.setText(String.valueOf(String.format("%.2f",Distancia)));
        textviewTempoMax.setText(String.valueOf(String.format("%.2f",Tempo_maximo)));
        textviewMoney.setText(String.valueOf(TaxadeLucroDoZe));
        logCars.setText(StringLogCars);

    }
    public void updateViewsCars() {
        textViewCarretaDisponiveis.setText(String.valueOf(primeiro_zezinho.getN_cars()[0]));
        textViewCarrosDisponiveis.setText(String.valueOf(primeiro_zezinho.getN_cars()[1]));
        textViewVansDisponiveis.setText(String.valueOf(primeiro_zezinho.getN_cars()[2]));
        textViewMotosDisponiveis.setText(String.valueOf(primeiro_zezinho.getN_cars()[3]));
    }
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        add_vehicles = (ImageButton) findViewById(R.id.Add_vehicles);
        textViewCarretaDisponiveis = findViewById(R.id.textViewCarretaDisponiveis);
        textViewCarretaOcupadas = findViewById(R.id.textViewCarretaOcupadas);
        textViewCarrosDisponiveis = findViewById(R.id.textViewCarrosDisponiveis);
        textViewCarrosOcupados = findViewById(R.id.textViewCarrosOcupados);
        textViewVansDisponiveis = findViewById(R.id.textViewVansDisponiveis);
        textViewVansOcupadas = findViewById(R.id.textViewVansOcupadas);
        textViewMotosDisponiveis = findViewById(R.id.textViewMotosDisponiveis);
        textViewMotosOcupadas = findViewById(R.id.textViewMotosOcupadas);

         textviewCustodeOperacao =  (TextView) findViewById(R.id.TextViewCustodeOperacao);
          imageviewCustoDeOperacao  = (ImageView)  findViewById(R.id.ImageViewCustoDeOperacao);
          textviewCustoeLucro = findViewById(R.id.textViewCustoeLucro);
         textviewTempoDeOpecacao = findViewById(R.id.textViewTempoDeOpecacao);
        texviewVeiculoOperacao = findViewById(R.id.TexViewVeiculoOperacao);
        buttonSelectMenorCusto = findViewById(R.id.ButtonSelectMenorCusto);

        textviewMaisRapido = findViewById(R.id. TextViewMaisRapido);
        imageviewMaisRapido = findViewById(R.id.ImageViewMaisRapido);
        textviewVeiculoMaisRapido = findViewById(R.id.textViewVeiculoMaisRapido);
        textviewTempoMaisRapido = findViewById(R.id.textViewTempoMaisRapido);
        texviewLucroMaisRapido = findViewById(R.id.TexViewLucroMaisRapido);
        bUttonTempo = findViewById(R.id.BUttonTempo);

        textviewmelhorCB = findViewById(R.id.TextViewMelhorCB);
        imageviewCB= findViewById(R.id.ImageViewCB);
        textviewCB= findViewById(R.id.textViewCB);
        textviewTempoCB= findViewById(R.id.textViewTempoCB);
        texviewVeiculoLucroCB = findViewById(R.id.TexViewVeiculoLucroCB);
        buttonSelectCB  = findViewById(R.id.ButtonSelectCB);


        textviewPeso = findViewById(R.id.textViewPeso);
        textviewDistancia = findViewById(R.id.textViewDistancia);
        textviewTempoMax = findViewById(R.id.textViewTempo);
        logCars = findViewById(R.id.LogCars);
        textviewMoney = findViewById(R.id.textViewMoney);



        Peso = 0.0; Tempo_maximo = 0.0; Distancia = 0.0;
        primeiro_zezinho = new Seu_ze();
        gridcars = findViewById(R.id.Gridcars);
        gridcars.animate().translationY(-1000).setDuration(10);
        loadData();
        InstanciaOsBagulho();
        updateViewsCars();
        updateViewsEncomendas();
        checa_condicoes();
    }
    @Override
    protected void onActivityResult(int requestCode , int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode,data);
        if(requestCode == 1){
            if(resultCode == RESULT_OK){
                Log.i("BUTTON","1");
                numero_de_carretas = data.getIntExtra("n_carretas",0 );
                numero_de_carros = data.getIntExtra("n_carros", 0);
                numero_de_vans = data.getIntExtra("n_vans", 0);
                numero_de_motos = data.getIntExtra("n_motos", 0);

                //setVehiclesTexts(primeiro_zezinho.getN_cars()[0],primeiro_zezinho.getN_cars()[1],primeiro_zezinho.getN_cars()[2],primeiro_zezinho.getN_cars()[3]);
                primeiro_zezinho.setN_cars(numero_de_carretas,numero_de_carros,numero_de_vans,numero_de_motos);
                InstanciaOsBagulho();

                updateViewsCars();

                funcoes_teste(primeiro_zezinho.getN_cars());

            }
        }
        if(requestCode == 2) {

            try {
                if (resultCode == RESULT_OK) {
                    Peso = data.getDoubleExtra("Peso_entrega", 0);
                    Distancia = data.getDoubleExtra("Distancia_entrega", 0);
                    Tempo_maximo = data.getDoubleExtra("TempoMax", 0);
                    setEncomendaTexts(Peso, Distancia, Tempo_maximo);
                    updateViewsEncomendas();
                    //Log.i("TESTECLIENTE","Peso , String, Distancia = "+ String.valueOf(Peso));
                    //Perfeitamente funcional :D
                    primeiro_zezinho.setPrecoDaViajem(Peso, Distancia, carreta_teste);
                    Custos[0] = primeiro_zezinho.getPrecoDeViajem();// Log.i("TESTEDEZE",String.valueOf(Custos[0]));
                    primeiro_zezinho.setPrecoDaViajem(Peso, Distancia, carro_teste);
                    Custos[1] = primeiro_zezinho.getPrecoDeViajem();//Log.i("TESTEDEZE",String.valueOf(Custos[1]));
                    primeiro_zezinho.setPrecoDaViajem(Peso, Distancia, van_teste);
                    Custos[2] = primeiro_zezinho.getPrecoDeViajem();//Log.i("TESTEDEZE",String.valueOf(Custos[2]));
                    primeiro_zezinho.setPrecoDaViajem(Peso, Distancia, moto_teste);
                    Custos[3] = primeiro_zezinho.getPrecoDeViajem();//Log.i("TESTEDEZE",String.valueOf(Custos[3]));
                    gridcars.animate().translationY(0).setDuration(4000);
                    checa_condicoes();
                    menorCustoDeVIajem();
                    menorTempoDeViajem();
                    MelhorCustoBeneficio();
                } else {
                    Toast.makeText(this, "Nada selecionado", Toast.LENGTH_SHORT).show();
                }
            }
            catch(Exception e){
                Toast.makeText(this, "Ops... Parece que algo deu errado, tente novamente com uma entrega diferente (;'", Toast.LENGTH_SHORT).show();
            }
        }
        if(requestCode == 3){
            if(resultCode == RESULT_OK){
                TaxadeLucroDoZe = data.getDoubleExtra("LUCRO" , 0);
                textviewMoney.setText(String.valueOf(TaxadeLucroDoZe));
            }
        }
    }
}

