package com.guipeeix7.matrix_tests_oo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.SeekBar;
import android.widget.Toast;

public class Add_vehicles extends AppCompatActivity {

    final int veiculos[] = new int[4];

    public void On_click_add(View view){
        Log.i("TESTE","3");
        Log.i("TESTE","Veiculos na posicao 2 (3)"+String.valueOf(veiculos[2]));
        Log.i("TESTE","Veiculos na posicao 2 (3)"+String.valueOf(veiculos[2]));

        Intent numbers = new Intent();
        numbers.putExtra("n_carretas",veiculos[0]);
        numbers.putExtra("n_carros",veiculos[1]);
        numbers.putExtra("n_vans",veiculos[2]);
        numbers.putExtra("n_motos",veiculos[3]);
        setResult(RESULT_OK , numbers);
        finish();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_vehicles);
        setTitle("Veiculos"); //Seta o nome da minha main activity

        ///////////////////////////?SEEK BARS VARIABLES////////////////////////////////////////
        final SeekBar MySeekBar0 = (SeekBar) findViewById(R.id.seekBar0); MySeekBar0.setMax(20);
        final SeekBar MySeekBar1 = (SeekBar) findViewById(R.id.seekBar1); MySeekBar1.setMax(20);
        final SeekBar MySeekBar2 = (SeekBar) findViewById(R.id.seekBar2); MySeekBar2.setMax(20);
        final SeekBar MySeekBar3 = (SeekBar) findViewById(R.id.seekBar3); MySeekBar3.setMax(20);

        Log.i("TESTE","2");
        MySeekBar0.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                veiculos[0] = progress;
            }
            @Override public void onStartTrackingTouch(SeekBar seekBar) {}
            @Override public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        MySeekBar1.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                veiculos[1] = progress;
            }
            @Override public void onStartTrackingTouch(SeekBar seekBar) {}
            @Override public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        MySeekBar2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                veiculos[2] = progress;
                Log.i("TESTE","Veiculos na posicao 2"+String.valueOf(veiculos[2]));
            }
            @Override public void onStartTrackingTouch(SeekBar seekBar) { }
            @Override public void onStopTrackingTouch(SeekBar seekBar) { }
        });

        Log.i("TESTE","Veiculos na posicao 2 (2)"+String.valueOf(veiculos[2]));

        MySeekBar3.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                veiculos[3] = progress;
            }
            @Override public void onStartTrackingTouch(SeekBar seekBar) { }
            @Override public void onStopTrackingTouch(SeekBar seekBar) { }
        });
        ////////////////////////////////////////////////////////////////////////////////////////
    }
}
