package com.guipeeix7.matrix_tests_oo;

public class Carro extends Veiculos{
    public Carro() {
        setTipo_de_veiculo("Carro");
        setRendimento(14);
        setCargaMaxima(360);
        setVelocidadeMedia(100);
        setPerda_de_rendimento(0.025);
        Define_combustivel("Gasolina", "Alcool");
        setDisponibilidade(1);
    }
}
