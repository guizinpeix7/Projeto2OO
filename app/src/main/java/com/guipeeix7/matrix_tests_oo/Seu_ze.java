package com.guipeeix7.matrix_tests_oo;

import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.SeekBar;
import android.widget.Toast;

public class Seu_ze{
    private EditText edittextlucro;

    private int n_cars[] = new int[4];
    private double PrecoDeViajem;

    Seu_ze() { }

    public void setN_cars(int nmCarreta, int nmCarros, int nmVans, int nmMotos){
        n_cars[0] = nmCarreta;
        n_cars[1] = nmCarros;
        n_cars[2] = nmVans;
        n_cars[3] = nmMotos;
    }
    public int[] getN_cars(){
        return n_cars;
    }
    public void setPrecoDaViajem(double Carga,double distancia, Veiculos tipoDeVeiculo){

        double preco_da_gasosa;
        //Log.i("PRECOS", tipoDeVeiculo.getTipo_de_veiculo());
        if(tipoDeVeiculo.getPreco_do_Combustivel_1() >  tipoDeVeiculo.getPreco_do_Combustivel_2()){
                preco_da_gasosa = tipoDeVeiculo.getPreco_do_Combustivel_2();
        }
        else preco_da_gasosa = tipoDeVeiculo.getPreco_do_Combustivel_1();
        //Log.i("PRECOS","Preco da gasosa ="+preco_da_gasosa);
        double Perda_deRend = tipoDeVeiculo.getGPerda_de_rendimento()*Carga;
        //Log.i("PRECOS","Perda de rendimento= " + Perda_deRend);
        double Rendimentop_Real = tipoDeVeiculo.getRendimento()-Perda_deRend;
        //Log.i("PRECOS","Rendimento real = " + Rendimentop_Real);
        //Log.i("PRECOS","rendimento do veiculo= " + tipoDeVeiculo.getRendimento());

        double Custo = (distancia/Rendimentop_Real)*preco_da_gasosa;
        //Log.i("PRECOS", "Custos = " + Custo);
        //Log.i("PRECOS" , "\n");

        PrecoDeViajem = Custo;
    }
    public double getPrecoDeViajem() {
        return PrecoDeViajem;
    }




}
