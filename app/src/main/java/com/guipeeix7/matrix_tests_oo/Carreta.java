package com.guipeeix7.matrix_tests_oo;
public class Carreta extends Veiculos{
    public Carreta() {
        setTipo_de_veiculo("Carreta");
        setRendimento(8);
        setCargaMaxima(30000);
        setVelocidadeMedia(60);
        setPerda_de_rendimento(0.0002);
        Define_combustivel("Disel");
        setDisponibilidade(1);
    }
}
