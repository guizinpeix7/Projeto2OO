package com.guipeeix7.matrix_tests_oo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import javax.xml.transform.Result;

public class DadosDoCLiente extends AppCompatActivity {

    private EditText edittextpeso;
    private EditText edittextdistancia;
    private EditText edittexttempoMax;



    public void On_click_set_encomenda(View view){
        Log.i("TESTECLIENTE", "2");
        if(edittextdistancia.getText().length() == 0 || edittextpeso.getText().length() == 0 || edittexttempoMax.getText().length() == 0){
            Toast.makeText(this, "Please insert all numbers", Toast.LENGTH_SHORT).show();
        }

        else{

            Log.i("TESTECLIENTE", "3");
            double peso = Double.parseDouble(edittextpeso.getText().toString());
            double distancia = Double.parseDouble(edittextdistancia.getText().toString());
            double tempomax = Double.parseDouble(edittexttempoMax.getText().toString());
            Intent backMain = new Intent(DadosDoCLiente.this,MainActivity.class);
            backMain.putExtra("Peso_entrega" , peso);
            backMain.putExtra("Distancia_entrega" , distancia);
            backMain.putExtra("TempoMax" , tempomax);

            setResult(RESULT_OK,backMain);
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dados_do_cliente);
        edittextpeso = findViewById(R.id.editTextPeso);
        edittextdistancia = findViewById(R.id.editTextDistancia);
        edittexttempoMax = findViewById(R.id.editTextTempoMax);
    }
}
