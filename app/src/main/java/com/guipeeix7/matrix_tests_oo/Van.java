package com.guipeeix7.matrix_tests_oo;
public class Van extends Veiculos{
    public Van() {
        setTipo_de_veiculo("Van");
        setRendimento(10);
        setCargaMaxima(3500);
        setVelocidadeMedia(60);
        setPerda_de_rendimento(0.0002);
        Define_combustivel("Disel");
        setDisponibilidade(1);
    }
}
